package ajaymehta.methodoverloadingsaves;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program1 {

    public static void main(String args[]) {

        Set<String> hs = new java.util.HashSet<>();

        // add elements in hashSet

        hs.add("One");
        hs.add("Two");
        hs.add("Three");
        hs.add("Four");
        hs.add("Five");

        // yeah yeahh ..for our all legal or illegal operation we have got methods to print ...we dont have to ...
        // write System.out  for evey method ...and also ..we dont have to care about how colud we pass different type of value
        // in one method with single data type like  print(String value)

        // lets print hashSet
        print(hs);  // click over method n click ctrl + P  ( geen will indicate ki konse method ne ye value li hai jo humne pass ki hai
        // n all yellow will indicate ...jisme ye value pass nai hue hai..
        // see we have taken  print(Set value) and print(hashSet value )  ...n humari ye value Set main gai hai ...HashSet main nai Wao...

        print(hs.size()); // presss ctrl +P  ..its gives value in primitve (int) wao ..we know that now..


        // chalo thoda time pass karte hai,,,

        print("Kalia");  // String took it

        print(4L);  // its a long value ..we havent created print(long value)  ..but still working  float took it..


        // Lets get back to work again....

        // return boolean value ....
        print(hs.add("diary"));  // Boolean (object) took it ...wait 1 min  ..we havnt defined Boolean ..shoot ..press again ctrl+P over method
        // it is showing Boolean method below Stack type method...hey thats genreic ..  Generic Types saved us..

        // so again hum just ek generic type ...method le to ... baki sare method na le to... we can ..we will see in next Program2

        // by the way (in terms of printing values)  not only generic method can save us ...Print(Object value) can save us too

        // i commented genreic  n Boolean value took by ...Object method..





    }  //  end of main method..





    public static void print(String value) {
        System.out.println(value);
    }


    public static void print(Integer value) {
        System.out.println(value);
    }


    public static void print(Float value) {
        System.out.println(value);
    }


    public static void print(Double value) {
        System.out.println(value);
    }

    public static void print(int value) {
        System.out.println(value);
    }


    public static void print(float value) {
        System.out.println(value);
    }


    public static void print(Set value) {
        System.out.println(value);
    }

    public static void print(List value) {
        System.out.println(value);
    }

    public static void print(ArrayList value) {
        System.out.println(value);
    }

    public static void print(HashSet value) {
        System.out.println(value);
    }

    public static void print(HashMap value) {
        System.out.println(value);
    }

    public static void print(Stack value) {
        System.out.println(value);
    }


    //TODo...uncomment one of the method from both .... u can use only one method in 1 class..

    // in the end we took generic type that stores all type of value..

/*    public static <T>void print(T value) {
        System.out.println(value);
    }*/


    public static void print(Object value) {
        System.out.println(value);
    }
}
