package ajaymehta.methodoverloadingsaves;

import java.util.Set;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program3 {


    public static void main(String args[]) {

        Set<String> hs = new java.util.HashSet<>();

        // add elements in hashSet

        hs.add("One");
        hs.add("Two");
        hs.add("Three");
        hs.add("Four");
        hs.add("Five");



        print(hs);
        print(hs.size()); // return int value  (primitive)


        print("Kalia");

        print(4L);



        print(hs.add("diary"));



    }  //  end of main method..

    // in terms of printing we dont need to use method overloading (in this case as i think)
   // same as object method..generic method will also do all the work...

    public static <T>void print(T value) { // our object can hold primitve value..too
        System.out.println(value);
    }
}
